<?php
require_once "logica/Administrador.php";
require_once "logica/Categoria.php";
require_once "logica/Talla.php";
require_once "logica/Producto.php";

$pid="";

if(isset($_GET["pid"])){
    
    $pid= base64_decode($_GET["pid"]);
}


?>

<!doctype html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Bootstrap CSS -->
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
	rel="stylesheet">
<!-- JS -->
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js">
</script>
<!-- CSS -->
<link rel="stylesheet" href="css/estilos.css"/>
<!-- iconos -->
<!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" >  -->
<script src="https://kit.fontawesome.com/518ebb38b4.js" ></script>
<!-- letra -->
<link href="https://fonts.googleapis.com/css2?family=Fredericka+the+Great&display=swap" rel="stylesheet"> 
 

<!-- Titulo pestaña con imágen -->
<title>Rompe tus límites, tienda deportiva</title>
<link rel="icon" type="image/png" href="img/gym-icon.png" />
</head>

<body>
<?php 

if($pid!=""){
    
    include $pid;
}else{
    
    include "presentacion/inicio.php";
}

?>


	
 <div class="container">
		<div class="row mt-3">
			<div class="row">
				<div class="col text-center text-muted" >
				<i class="fas fa-skull "></i> Y &copy; <?php echo date("Y") ?>	
				</div>			
			</div>
		</div>
		
		
	</div>	
	
	
	
	

</body>
</html>



