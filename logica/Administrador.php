<?php
class Administrador{
    
    private $id_administrador;
    private $nombre;
    private $conexion;
    private $administradorDAO;
    
    
    
    
    
    
    /**
     * @return number
     */
    public function getId_administrador()
    {
        return $this->id_administrador;
    }

  
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @return Conexion
     */
    public function getConexion()
    {
        return $this->conexion;
    }

    /**
     * @return AdministradorDAO
     */
    public function getAdministradorDAO()
    {
        return $this->administradorDAO;
    }

    /**
     * @param number $id_administrador
     */
    public function setId_administrador($id_administrador)
    {
        $this->id_administrador = $id_administrador;
    }

    /**
     * @param Ambigous <string, mixed> $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @param Conexion $conexion
     */
    public function setConexion($conexion)
    {
        $this->conexion = $conexion;
    }

    /**
     * @param AdministradorDAO $administradorDAO
     */
    public function setAdministradorDAO($administradorDAO)
    {
        $this->administradorDAO = $administradorDAO;
    }

    
    public function Administrador($id_administrador=0,$nombre=""){
        
        $this->id_administrador=$id_administrador;
        $this->nombre=$nombre;
        $this->conexion=new Conexion();    
        $this->administradorDAO=new AdministradorDAO( $this->id_administrador, $this->nombre);
    
    }
    
    
    //comilla simple para sql y doble para php, las ultimas comillas es para cerrar la consulta
    public function consultar(){
        $this -> conexion -> abrir();      //abro conexion
        $this -> conexion -> ejecutar($this -> administradorDAO -> consultar()); //ejecuto sentencia sql
        $resultado=$this->conexion->extraer(); //recibo nombre del administrador
        $this->nombre=$resultado[0]; //0 porque el id es autonumerico
    }
    
    
    
    
    
}



?>