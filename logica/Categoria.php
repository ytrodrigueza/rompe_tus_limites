<?php

class Categoria {
    
    private $id_categoria;
    private $nombre_categoria;
    private $conexion;
    private $categoriaDAO;
    
    
   
    
    
    /**
     * @return number
     */
    public function getId_categoria()
    {
        return $this->id_categoria;
    }

  
    public function getNombre_categoria()
    {
        return $this->nombre_categoria;
    }

    /**
     * @param number $id_categoria
     */
    public function setId_categoria($id_categoria)
    {
        $this->id_categoria = $id_categoria;
    }

    /**
     * @param Ambigous <string, mixed> $nombre_categoria
     */
    public function setNombre_categoria($nombre_categoria)
    {
        $this->nombre_categoria = $nombre_categoria;
    }

    public function Categoria($id_categoria=0,$nombre_categoria=""){
        
        $this->id_categoria=$id_categoria;
        $this->nombre_categoria=$nombre_categoria;
        $this -> conexion = new Conexion();
        $this -> categoriaDAO = new CategoriaDAO( $this->id_categoria, $this->nombre_categoria);
        
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> categoriaDAO -> consultar());
        $resultado = $this -> conexion -> extraer();
        $this -> nombre_categoria = $resultado[0];
        $this -> conexion -> cerrar();
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> categoriaDAO -> consultarTodos());
        $categorias = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($categorias, new Categoria($resultado[0], $resultado[1]));
        }
        $this -> conexion -> cerrar();
        return $categorias;
    }
    
    
    
    
    
    
    
}
?>