<?php

class Producto {
    
    private $id_producto;
    private $precio;
    private $nombre;
    private $fk_id_categoria;
    private $fk_id_talla;
    private $fk_id_administrador;
    private $conexion;
    private $productoDAO;
    
    

    
    
    /**
     * @return number
     */
    public function getId_producto()
    {
        return $this->id_producto;
    }

    /**
     * @return string
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @return string
     */
    public function getFk_id_categoria()
    {
        return $this->fk_id_categoria;
    }

    /**
     * @return string
     */
    public function getFk_id_talla()
    {
        return $this->fk_id_talla;
    }

    /**
     * @return string
     */
    public function getFk_id_administrador()
    {
        return $this->fk_id_administrador;
    }

    /**
     * @return Conexion
     */
    public function getConexion()
    {
        return $this->conexion;
    }

    /**
     * @return ProductoDAO
     */
    public function getProductoDAO()
    {
        return $this->productoDAO;
    }

    /**
     * @param number $id_producto
     */
    public function setId_producto($id_producto)
    {
        $this->id_producto = $id_producto;
    }

    /**
     * @param string $precio
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;
    }

    /**
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @param string $fk_id_categoria
     */
    public function setFk_id_categoria($fk_id_categoria)
    {
        $this->fk_id_categoria = $fk_id_categoria;
    }

    /**
     * @param string $fk_id_talla
     */
    public function setFk_id_talla($fk_id_talla)
    {
        $this->fk_id_talla = $fk_id_talla;
    }

    /**
     * @param string $fk_id_administrador
     */
    public function setFk_id_administrador($fk_id_administrador)
    {
        $this->fk_id_administrador = $fk_id_administrador;
    }

    /**
     * @param Conexion $conexion
     */
    public function setConexion($conexion)
    {
        $this->conexion = $conexion;
    }

    /**
     * @param ProductoDAO $productoDAO
     */
    public function setProductoDAO($productoDAO)
    {
        $this->productoDAO = $productoDAO;
    }

    //constructor
    public function ProductoDAO($id_producto=0,$precio="",$nombre="",$fk_id_categoria="",$fk_id_talla="",$fk_id_administrador=""){
        
        //emulando sobrecarga
        
        $this->id_producto=$id_producto;
        $this->precio=$precio;
        $this->nombre=$nombre;
        $this->fk_id_categoria=$fk_id_categoria;
        $this->fk_id_talla=$fk_id_talla;
        $this->fk_id_administrador=$fk_id_administrador;
        $this -> conexion = new Conexion();
        $this -> productoDAO = new ProductoDAO($this->id_producto, $this->precio,  $this->nombre,  $this->fk_id_categoria,   $this->fk_id_talla, $this->fk_id_administrador);
    }
    
    
    
    
    //metodo para crear producto
    public function agregarProducto(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> agregarProducto());
        $this -> conexion -> cerrar();
    }
    
    
    
    
    
    
    
    
}

?>