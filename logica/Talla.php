<?php
class Talla{
    
    private $id_talla;
    private $numero_talla;
    private $conexion;
    private $tallaDAO;
    
    
    /**
     * @return number
     */
    public function getId_talla()
    {
        return $this->id_talla;
    }

    /**
     * @return string
     */
    public function getNumero_talla()
    {
        return $this->numero_talla;
    }

  
   
    public function setId_talla($id_talla)
    {
        $this->id_talla = $id_talla;
    }

    /**
     * @param string $numero_talla
     */
    public function setNumero_talla($numero_talla)
    {
        $this->numero_talla = $numero_talla;
    }

    public function TallaDAO($id_talla=0,$numero_talla=""){
        $this->id_talla=$id_talla;
        $this->numero_talla=$numero_talla;
        $this -> conexion = new Conexion();
        $this->tallaDAO=new TallaDAO($this->id_talla, $this->numero_talla);
        
    }
    
    public function consultar(){
        return "select numero_talla
                from talla
                where id_talla = '" . $this -> id_talla."'";
    }
    
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> tallaDAO -> consultarTodos());
        $tallas = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($tallas, new Categoria($resultado[0], $resultado[1]));
        }
        $this -> conexion -> cerrar();
        return $tallas;
    }
    
    
    
    
    
    
}



?>