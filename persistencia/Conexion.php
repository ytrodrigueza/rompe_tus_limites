<?php
class Conexion{
    
    private $mysqli; //hasta php 7, a partir del 8 es mysql
    private $resultado;
    
    //abrir conexion
    public function abrir(){
        $this -> mysqli = new mysqli("localhost", "root", "", "rompe_tus_limites");
        $this -> mysqli -> set_charset("utf8");
    }
    
    //cerrar, por defecto es publico
    public function cerrar(){
        $this -> mysqli -> close();
    }
    
    //ejecutar sentencia SQL
    public function ejecutar($sentencia){
        $this -> resultado = $this -> mysqli -> query($sentencia);
    }
    
    
    //obtener los registros
    public function extraer(){
        return $this -> resultado -> fetch_row();
        //recorre la siguiente fila de la tabla , retorna toda la fila
    }
    
    //contar filas de los resultado
    public function numFilas(){
        return ($this -> resultado != null) ? $this -> resultado -> num_rows : 0;
        
    }
}
?>